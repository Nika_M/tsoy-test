var $name               = "init";

const { src, dest, watch } = require('gulp');

path = {
    'plugin' : './public_html/wp-content/plugins/' + $name + '/',
    'theme' : './public_html/wp-content/themes/' + $name + '/'
}

// Подключаемые плагины
const sass              = require('gulp-sass');
const autoprefixer      = require('gulp-autoprefixer');
const sourcemaps        = require('gulp-sourcemaps');

const concat            = require('gulp-concat');
const uglify            = require('gulp-uglify-es').default;


// Функция для обработки Стилей в теме
function theme_styles() {

    return src( path.theme + 'scss/theme.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({                                                    // Конвертация sass в css
            errorLogToConsole: true,                                    // Проверка и вывод ошибок в консоль
            outputStyle: 'compressed'                                   // Минифицируем код
        }))
        .on( 'error', console.error.bind(console) )
        .pipe(autoprefixer({                                            // Проставляем префиксы для css
            overrideBrowserslist: ['last 2 version'],                   // Поддержка версии браузеров
            cascade: false
        }))
        .pipe( sourcemaps.write('../css') )
        .pipe( dest( path.theme + 'css/') );

}

// Функция для обработки Стилей в Админке
function admin_styles() {

    return src( path.plugin + 'assets/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({                                                    // Конвертация sass в css
            errorLogToConsole: true,                                    // Проверка и вывод ошибок в консоль
            outputStyle: 'compressed'                                   // Минифицируем код
        }))
        .on( 'error', console.error.bind(console) )
        .pipe(autoprefixer({                                            // Проставляем префиксы для css
            overrideBrowserslist: ['last 2 version'],                   // Поддержка версии браузеров
            cascade: false
        }))
        .pipe( sourcemaps.write() )
        .pipe( dest( path.plugin + 'assets/css') );

}

// Функция для обработки Скриптов
function scripts() {
    return src( path.theme + 'js/scripts/custom/*.js')
        .pipe(sourcemaps.init())
        .pipe( concat( "scripts.js" ) )
        .pipe( uglify({
            toplevel: true
        }) )
        .pipe( sourcemaps.write('../js') )
        .pipe( dest( path.theme + 'js') );
}


// Слушаем файлы
function watchFiles() {
    watch( path.plugin + "assets/scss/*.scss", admin_styles );
    watch( path.theme + "scss/**/*.scss", theme_styles );
    watch( path.theme + "js/scripts/**/*.js", scripts );
}

exports.default = watchFiles;