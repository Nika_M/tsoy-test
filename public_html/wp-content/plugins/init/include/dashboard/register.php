<?php


    function user_check_code(){

        $data = $_POST['data'];        

        $check_user = new Users( $data );

        $check_user->user_check_code();

        die();
    }

    add_action('wp_ajax_user_check_code', 'user_check_code');
    add_action('wp_ajax_nopriv_user_check_code', 'user_check_code');


    function check_user(){ 
        
        $data = $_POST['data'];

        $check_user = new Users( $data );

        $check_user->check_fields_register_user();

        die();
    }

    add_action('wp_ajax_check_user', 'check_user');
    add_action('wp_ajax_nopriv_check_user', 'check_user');

    function register_user(){ 
        
        $data = $_POST['data'];

        $check_user = new Users( $data );

        $check_user->register_user();

        die();
    }

    add_action('wp_ajax_register_user', 'register_user');
    add_action('wp_ajax_nopriv_register_user', 'register_user');

?>