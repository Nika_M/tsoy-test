<?php

function edit_profile(){

    if( $_POST['data'] ){
        $data = $_POST['data'];
        $user_id = get_current_user_id();
        $coords = json_encode( $data['coords'] );

        update_user_meta( $user_id, 'first_name', $data['name'] );
        update_user_meta( $user_id, 'last_name', $data['surname'] );
        update_user_meta( $user_id, 'phone', $data['phone'] );
        update_user_meta( $user_id, 'additional_phone', $data['additional_phone'] );
        update_user_meta( $user_id, 'address', $data['address'] );
        update_user_meta( $user_id, 'coords', $coords );

        global $wpdb;

        if( $data['email'] ){

            $wpdb->update( 'itqkz_users',
                array( 'user_email' => $data['email']),
                array( 'ID' => $user_id ),
                array( '%s' ),
                array( '%d' )
            );

            echo json_encode(array('result' => 'success'));

        }else{
            echo json_encode(array('result' => 'error'));
        }
    }else{
        echo json_encode(array('result' => 'error'));
    }

    die();
}
add_action( 'wp_ajax_edit_profile', 'edit_profile' );
add_action( 'wp_ajax_nopriv_edit_profile', 'edit_profile' );