<?php

class Users {

    public $error_container = [];
    public $phone = "";
    public $userdata = "";

    function __construct($data) {
        $this->data = $data;
    }

    // Функция проверки полей
    function check_field($data, $check_type) {
        
        $type = $data[2];
        $value = $data[0];
        $required = $data[3] == "true";
        global $wpdb;  

        if($required){ 

            if($value == ''){
                return 'Заполните поле';
            }

            if($type == 'phone'){
                
                if( strpos($value, '_') ){
                    return 'Заполните поле';
                }

                $value = unmask_phone($value);
                if(mb_strlen($value) < 11){
                    return 'Заполните поле';
                }

                if($check_type == 'register'){

                    // Проверка номера в базе данных 
                    $query = $wpdb->get_results( "SELECT `ID` FROM `$wpdb->users` WHERE `user_login` = '$value'");

                    if( $query ){
                        return 'Данный номер уже зарегистрирован';
                    }
                }

            }else if($type == 'email'){

                // Проверка Email на валидность
                if ( !filter_var($value, FILTER_VALIDATE_EMAIL) ){
                    return 'Email указан неверно';
                }

                if($check_type == 'register'){
                    // Проверка номера в базе данных 
                    $query = $wpdb->get_results( "SELECT `ID`, `user_login` FROM `$wpdb->users` WHERE `user_email` = '$value'");

                    if( $query ){
                        return 'Данный email уже зарегестрирован';
                    }
                }

            }else if($type == 'confirm_pass'){                
                $password = $this->data['password'][0];

                if($value != $password){
                    return 'Пароли не совпадают';
                }

            }else if($type == 'checkbox'){                

                if(!$value){
                    return 'Вы не приняли условия соглашения.';
                }

            }
            
        }

    }
    
    // Проверки при Регистрация
    function check_fields_register_user() {

        // Запуск проверки полей
        foreach($this->data as $data){

            $response = $this->check_field($data, 'register');

            if($response){
                $error_container[$data[1]] = $response;
            }

        }

        // Проверка на ошибки
        if( !empty($error_container)){
            // если есть ошибки то отправляем
            echo json_encode( array('result' => 'error', 'text_error' => $error_container) );
            
        }else{
            
            $this->send_sms("register");
            $this->userdata = $this->data;
        }    

    }

    function check_fields_restore_user() {

        // Запуск проверки полей
        foreach($this->data as $data){

            $response = $this->check_field($data, 'restore');

            if($response){
                $error_container[$data[1]] = $response;
            }

        }

        // Проверка на ошибки
        if( !empty($error_container)){
            // если есть ошибки то отправляем
            echo json_encode( array('result' => 'error', 'text_error' => $error_container) );
            
        }else{
            $this->check_restore_user();
        }    
    }

    function check_fields_restore_password() {

        // Запуск проверки полей
        foreach($this->data as $key => $data){

            $response = $this->check_field($data, 'restore');

            if($response){
                $error_container[$data[1]] = $response;
            }

        }

        // Проверка на ошибки
        if( !empty($error_container)){
            // если есть ошибки то отправляем
            echo json_encode( array('result' => 'error', 'text_error' => $error_container) );
            
        }else{
            $this->restore_password();
        }    
    }
    
    // Проверки при Авторизации
    function check_fields_auth_user() {

        // Запуск проверки полей
        foreach($this->data as $data){

            $response = $this->check_field($data, 'login');

            if($response){
                $error_container[$data[1]] = $response;
            }

        }

        // Проверка на ошибки
        if( !empty($error_container)){
            // если есть ошибки то отправляем
            echo json_encode( array('result' => 'error', 'text_error' => $error_container) );
            
        }else{
            $this->auth_user();
        }    

    }

    // Отправка СМС при регистрации
    function send_sms($type) {

        $phone = unmask_phone( $this->data['phone'][0] );            

        $data = [
            "phone"  => $phone,
            "type"   => $type
        ];

        $send_sms = new SendSms($data);
        $send_sms->send();

    }

    function register_user() {

        global $wpdb;

        $data = $this->data['info'];            

        $phone = unmask_phone( $data['phone'][0] );

        $userdata = array(
            'user_pass'       => $data['password'][0], 
            'user_login'      => $phone,
            'user_email'      => $data['email'][0],
            'first_name'      => $data['name'][0],
            'last_name'       => $data['surname'][0],
            // 'role'            => '',
            // 'user_nicename'   => '',
            // 'nickname'        => '',
            // 'display_name'    => '',
        );

         // Регистрация пользователя 
        $user_id = wp_insert_user( $userdata );

        // Проверка ошибок при регистрации
        if ( is_wp_error( $user_id ) ) {
                
            if( $user_id->get_error_code() == 'existing_user_login' ){
                $error_container['register_phone'] = 'Этот номер телефона уже зарегистрирован!';
            }elseif( $user_id->get_error_code() == 'existing_user_email' ){
                $error_container['register_email'] = 'Этот e-mail уже используется!';
            }else{
                $error_container['errors']  = $user_id->get_error_message();
            }

            // если есть ошибки то отправляем
            echo json_encode(array('result' => 'error', 'text_error' => $error_container));

        } else {

            wp_set_current_user( $user_id, $phone );
            wp_set_auth_cookie( $user_id );
            do_action( 'wp_login', $phone );

            $wpdb->update( 'init_users_sms_code',
                array( 'code' => '', 'hash_key' => '' ),
                array( 'phone' => $phone ),
                array( '%s', '%s' ),
                array( '%s' )
            );

            // Если нет ошибок сообщаем об успехе
            echo json_encode( array('result' => 'success') );

        }

    }

    function auth_user() {

        $data = $this->data;

        $phone = unmask_phone( $data['phone'][0] );

        $userdata = array(
            'user_login'    => $phone,
            'user_password' => $data['password'][0],
            'remember'      => $data['remember'][0]
        );

        // Авторизация
        $user = wp_signon( $userdata, true ); // true если сайт с Сертификатом SSL

        // Корректируем ошибки
        if ( is_wp_error($user) ) {  

            if( $user->get_error_code() == 'empty_username' ){
                $errorContainer['login_phone'] = 'Вы не правильно ввели номер телефона';
            } elseif( $user->get_error_code() == 'incorrect_password'){
                $errorContainer['login_password']  = 'Вы ввели неверный пароль';
            } elseif( $user->get_error_code() == 'invalid_username'){
                $errorContainer['login_phone']  = 'Вы ввели неверный номер';
            } else{
                $errorContainer['errors']  = $user->get_error_message();
            }

            echo json_encode(array('result' => 'error', 'text_error' => $errorContainer));

        }else{
            // Если нет ошибок сообщаем об успехе
            echo json_encode(array('result' => 'success'));

        }

    }

    function restore_password() {
        global $wpdb;

        $data = $this->data;

        $key        = $data['key'][0];
        $password   = $data['password'][0];
        $type       = $data['type'][0];

        if($type == "phone"){
            $db_name    = 'init_users_sms_code';
            $select     = 'phone';
            $field_name = 'user_login';
        }elseif( $type == "email" ){
            $db_name    = 'init_users_email_code';
            $select     = 'email';
            $field_name = 'user_email';
        }

        $selected_field  = $wpdb->get_var( "SELECT `$select` FROM `$db_name` WHERE `hash_key` = '$key'" );
        $user_id    = $wpdb->get_var( "SELECT `ID` FROM `init_users` WHERE `$field_name` = '$selected_field'" );

        $update_password = wp_update_user( [ 
            'ID'            => $user_id,
            'user_pass'     => $password
        ] );

        if( $update_password == $user_id ){

            if($type == "phone"){
                $wpdb->update( $db_name,
                    array( 'code' => '', 'hash_key' => '' ),
                    array( $select => $selected_field ),
                    array( '%s', '%s' ),
                    array( '%s' )
                );
            }elseif( $type == "email" ){
                $wpdb->update( $db_name,
                    array('hash_key' => '' ),
                    array( $select => $selected_field ),
                    array( '%s' ),
                    array( '%s' )
                );
            }

            echo json_encode(array('result' => 'success'));

        }else{

            echo json_encode(array('result' => 'error'));
            
        }
        

    }

    function check_restore_user() {

        global $wpdb;

        $data = $this->data;

        if( isset($data['phone']) ){
            
            $phone = unmask_phone( $data['phone'][0] );                

            // Проверка номера в базе данных 
            $query = $wpdb->get_row( "SELECT `ID` FROM `$wpdb->users` WHERE `user_login` = '$phone'");

            if( $query ){
                $this->send_sms("restore");
            }else{
                $errorContainer[$data['phone'][1]] = 'Данный номер не зарегистрирован';
                echo json_encode(array('result' => 'error', 'text_error' => $errorContainer));
            }

        }else{
            $email = $data['email'][0];               

            // Проверка номера в базе данных 
            $query = $wpdb->get_row( "SELECT `ID` FROM `$wpdb->users` WHERE `user_email` = '$email'");

            if( $query ){

                $user_data = $wpdb->get_var( "SELECT `id` FROM `init_users_email_code` WHERE `email` = '$email' ORDER BY `id` DESC" );

                $user_data_id = "";
                $headers = array("content-type: text/html");
                
                // Генерируем шести значное число
                $code = mt_rand(100000, 999999);

                $hash_key = generate_hash_key();

                $user_info = get_user_by_email( $email );

                $user_name = "$user_info->first_name $user_info->last_name";

                $messages = mail_template( "Восстановление пароля", "<table><tr>
                <td>2</td><td>1</td>
                </tr></table>");

                // $messages = recovery_send_mail( $user_name, $email, $hash_key );

                // Если номер в базе
                if( $user_data ){
                    $user_data_id = $user_data;
                }

                $wpdb->replace( 'init_users_email_code', array( 
                    'id'        => $user_data_id,
                    'email'     => $email,
                    'hash_key'  => $hash_key
                ) );

                $send_mail = wp_mail( $email, "Восстановление пароля на сайте ITQ.kz", $messages, $headers );

                // Если нет ошибок сообщаем об успехе
                if($send_mail){
                    echo json_encode(array('result' => 'success', 'type' => 'email'));
                }else{
                    echo json_encode(array('result' => 'error'));
                }



            }else{
                $errorContainer[$data['email'][1]] = 'Данный email не зарегистрирован';
                echo json_encode(array('result' => 'error', 'text_error' => $errorContainer));
            }
        }

    }

    function user_check_code(){

        global $wpdb;

        $data = $this->data;

        $type       = $data['type'];
        $user_phone = $data['phone'];
        $code      = (int)$data['value'];
        $user_data = $wpdb->get_row( "SELECT * FROM `init_users_sms_code` WHERE `phone` = '$user_phone' AND `type` = '$type' ORDER BY `id` DESC" );

        if( $code == $user_data->code ){                

            if($type == 'register'){
                $this->register_user();
            }else if($type == 'restore'){
                echo json_encode( array( 'result' => 'success', 'key' => $user_data->hash_key ) );
            }

        }else{
            echo json_encode(array('result' => 'error'));
        }
    }

}

add_filter( 'lostpassword_url', 'change_lostpassword_url', 10, 2 );

function change_lostpassword_url( $url, $redirect ){
	$new_url = home_url( '/test_reset_pass' );
	return add_query_arg( array('redirect'=>$redirect), $new_url );
}

function unmask_phone($phone) {

    // Масив из символов, которые нужно удалить
    $removable_symbols = array( '+', '(', ')', ' ', '-', '_');
    // Удаление лишних символов
    $phone = str_replace($removable_symbols, '', trim($phone));

    return $phone;
    
}

function generate_hash_key(){
    $hash = hash_init('md5');
    $key = rand(1000000000, 9999999999);
    hash_update($hash, $key);
    $hash = hash_final($hash);

    return $hash;
}