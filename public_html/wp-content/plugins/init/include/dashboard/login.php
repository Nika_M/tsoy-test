<?php

    function login_user(){ 
            
        $data = $_POST['data'];

        $check_user = new Users( $data );

        $check_user->check_fields_auth_user();

        die();
    }
    add_action('wp_ajax_login_user', 'login_user');
    add_action('wp_ajax_nopriv_login_user', 'login_user');

    
    function redirect_login_page() {  
        $login_page  = home_url( '/login/' );  
        $page_viewed = basename($_SERVER['REQUEST_URI']);  

        if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {  
            wp_redirect($login_page);  
            exit;  
        }  
        if ( is_admin() && ! current_user_can( 'administrator' ) && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
            wp_redirect( '/login/' );
            exit;
        }
    }
    add_action('init','redirect_login_page');


    function logout_redirect(){
        wp_safe_redirect( site_url( '/login/' ) );
        exit;
    }
    add_action( 'wp_logout', 'logout_redirect', 5 );


    function logout_function(){
        wp_logout();
        exit;
    }
    add_action('wp_ajax_logout_function', 'logout_function');
    add_action('wp_ajax_nopriv_logout_function', 'logout_function');