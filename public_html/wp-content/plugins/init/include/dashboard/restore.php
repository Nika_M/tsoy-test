<?php

    function get_restore(){
        if( isset( $_GET['email'] ) && isset( $_GET['token'] ) ){
            global $wpdb;
            $email = $_GET['email'];
            $hash_key = $_GET['token'];
            
            $user_data = $wpdb->get_row( "SELECT * FROM `itqkz_users_email_code` WHERE `email` = '$email' AND `hash_key` = '$hash_key' ORDER BY `id` DESC" );  

            if( $user_data ){
                return array( 'result' => 'success', 'key' => $hash_key );
            }

        }
    }
    

    function restore_user(){ 
        
        $data = $_POST['data'];

        $check_user = new Users( $data );

        $check_user->check_fields_restore_user();

        die();
    }
    add_action('wp_ajax_restore_user', 'restore_user');
	add_action('wp_ajax_nopriv_restore_user', 'restore_user');
    
    
	function restore_password(){
        
        $data = $_POST['data'];

        $check_user = new Users( $data );

        $check_user->check_fields_restore_password();

        die();
	}
	add_action('wp_ajax_restore_password', 'restore_password');
	add_action('wp_ajax_nopriv_restore_password', 'restore_password');

?>