<?php 

/**
 * Создаем произвольный тип записи Товар
 */
add_action( 'init', 'register_post_type_products' );

function register_post_type_products() {
	$labels = array(
		'name' => 'Товары',
		'singular_name' => 'Товар', // админ панель Добавить->Функцию
		'menu_name' => 'Товары', // ссылка в меню в админке
		'add_new' => 'Добавить товар',
		'add_new_item' => 'Добавить новый товар', // заголовок тега <title>
	);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'show_ui' => true, // показывать интерфейс в админке
		'has_archive' => true, 
		'menu_icon' => 'dashicons-cart', // иконка в меню
		'menu_position' => 3, // порядок в меню
		'supports' => array( 'title', 'editor', 'author', 'thumbnail')
	);
	register_post_type('product', $args); //porfolio это ссылка в url
}