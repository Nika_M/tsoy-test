<?php

function add_reviews(){

    global $wpdb;

    $user_id = get_current_user_id();

    if( !empty( $_POST['data'] ) ){
        $data = $_POST['data'];
        $rating = $data['rating'];
        $review   = $data['review'];
        
        $add_reviews = $wpdb->insert(
            'init_custom_reviews',
            array(
                'user_id'   => $user_id,
                'rating'    => $rating,
                'review'    => $review,
                'status'        => 0
            ),
            array( '%d', '%d', '%s', '%d' )
        );

        // Если нет ошибок сообщаем об успехе
        if( $add_reviews ){
            echo json_encode(array('result' => 'success'));
        }else{
            echo json_encode(array('result' => 'error'));
        }
    }

    die();
}
add_action('wp_ajax_add_reviews', 'add_reviews');
add_action('wp_ajax_nopriv_add_reviews', 'add_reviews');

// Функция Удаление отзывов со сменой статуса
function reviews_delete_callback(){

    // Подключаем глобальную переменую для "Базы Данных"
    global $wpdb;

    // Проверка если нет не одной галочки
    if( isset( $_POST['string_id'] ) ){
        
        // Получаем строку с ID
        $stringID = $_POST['string_id'];
        
        // Преобразуем в массив
        $array = explode(',', $stringID);
        
        // Узнаем колличество ID
        $countID = count($array);
        
        // Удаление с базы
		$wpdb->query( "UPDATE `init_custom_reviews` SET `status` = 2 WHERE `id` IN ($stringID)" );
        
        // Выводим правильное уведомление
        if($countID > 1){
            echo "Выбранные отзывы удалены";
        }else{
            echo "Выбранный отзыв удален";
        }
    }

    wp_die();
}
add_action('wp_ajax_reviews_delete', 'reviews_delete_callback');
add_action('wp_ajax_nopriv_reviews_delete', 'reviews_delete_callback');

// Функция Удаление отзывов с корзины
function reviews_trash_delete_callback() {

    // Подключаем глобальную переменую для "Базы Данных"
    global $wpdb;

    // Проверка если нет не одной галочки
    if( isset( $_POST['string_id'] ) ){
		
		// Получаем строку с ID
        $stringID = $_POST['string_id'];
		
		// Преобразуем в массив
		$array = explode(',', $stringID);
		
		// Узнаем колличество ID
		$countID = count($array);
        
        // Удаление с базы
		$wpdb->query( "DELETE FROM `init_custom_reviews` WHERE `id` IN ($stringID)" );
		
		// Выводим правильное уведомление
		if($countID > 1){
			echo "Выбранные заявки удалены";
		}else{
			echo "Выбранная заявка удалена";
		}
    }

	wp_die();
}
add_action('wp_ajax_reviews_trash_delete', 'reviews_trash_delete_callback');
add_action('wp_ajax_nopriv_reviews_trash_delete', 'reviews_trash_delete_callback');

// Функция Восстановление отзывов с корзины
function reviews_recover_callback() {

    // Подключаем глобальную переменую для "Базы Данных"
    global $wpdb;

    // Проверка если нет не одной галочки
    if( isset( $_POST['string_id'] ) ){
		
		// Получаем строку с ID
        $stringID = $_POST['string_id'];
		
		// Преобразуем в массив
		$array = explode(',', $stringID);
		
		// Узнаем колличество ID
		$countID = count($array);
        
        // Восстановление с базы
		$wpdb->query( "UPDATE `init_custom_reviews` SET `status` = 0 WHERE `id` IN ($stringID)" );
		
		// Выводим правильное уведомление
		if($countID > 1){
			echo "Выбранные заявки удалены";
		}else{
			echo "Выбранная заявка удалена";
		}
    }

	wp_die();
}
add_action('wp_ajax_reviews_recover', 'reviews_recover_callback');
add_action('wp_ajax_nopriv_reviews_recover', 'reviews_recover_callback');

// Обработка отзывов для AJAX
function reviews_process_callback() {

    // Подключаем глобальную переменую для "Базы Данных"
	global $wpdb;
    
    // Проверка если нет не одной галочки
    if( isset( $_POST['string_id'] ) ){
		
		// Получаем строку с ID
        $stringID = $_POST['string_id'];
		
		// Преобразуем в массив
		$array = explode(',', $stringID);
		
		// Узнаем колличество ID
        $countID = count($array);
        
        // Удаление с базы
        $wpdb->query( "UPDATE `init_custom_reviews` SET `status` = 1, `date_processing` = CURRENT_TIME WHERE `id` IN ($stringID)" );
		
		// Выводим правильное уведомление
		if($countID > 1){
			echo "Выбранные отзывы обработаны";
		}else{
			echo "Выбранный отзыв обработан";
		}
    }

	wp_die();
}
add_action('wp_ajax_reviews_process', 'reviews_process_callback');
add_action('wp_ajax_nopriv_reviews_process', 'reviews_process_callback');