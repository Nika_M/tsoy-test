$('.certificates').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    prevArrow:"<i class='icon-left'></>",
    nextArrow:"<i class='icon-right'></>",
    centerMode: true,
    variableWidth: true
});

$('.reviews-block').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: false,
    dots: true
});
