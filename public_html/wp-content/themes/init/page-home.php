<?php

/**
 * Template Name: Home Page
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package init
 */

get_header();
?>

<section class="first-slide">
    <!-- <section class="first-slide"> -->
    <div class="container">
        <div class="wrapper">
            <div class="first-description">
                <h1>Диагностика и лечение<br> женских заболеваний в Алматы</h1>
                <p>Восстановите свое женское здоровье без страха и стеснения</p>
                <button class="btn">Оставить заявку</button>
            </div>
            <div class="first-img" style="background-image:url(/wp-content/themes/init/img/doctor.jpg);"></div>
        </div>
    </div>
</section>

<section class="about">
    <div class="container">
        <div class="wrapper">
            <div class="about-img" style="background-image:url(/wp-content/themes/init/img/doctor.jpg);"></div>
            <div class="about-me">
                <h3>Цой Евгения</h3>
                <h6>Акушер-гинеколог, гинеколог-хирург высшей категории</h6>
                <ul>
                    <li>
                        <i class="icon-medal"></i>
                        <div class="text">
                            <h4>16 лет опыта</h4>
                            <p>Практический опыт с 2004 года</p>
                        </div>
                    </li>

                    <li>
                        <i class="icon-medal"></i>
                        <div class="text">
                            <h4>Врач высшей категории</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                    </li>

                    <li>
                        <i class="icon-medal"></i>
                        <div class="text">
                            <h4>Lorem ipsum dolor sit</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="diseases">
    <div class="container">
        <div class="wrapper">
            <div class="title">

                <!-- <h2>Помогу с лечением</h2> -->
                <h3>Помогу с лечением</h3>
                <p>Кроме ведения ЭКО и беременности</p>
            </div>
            <div class="types-deseases">
                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">

                        <h6>Эндометриоз</h6>
                    </div>
                </div>
                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>Миома матки</h6>
                    </div>
                </div>

                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>Бесплодие</h6>
                    </div>
                </div>

                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>Полипы</h6>
                    </div>
                </div>

                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>Киста яичника</h6>
                    </div>
                </div>

                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>Сальпингит</h6>
                    </div>
                </div>

                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>ЗППП</h6>
                    </div>
                </div>

                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>Опущение матки</h6>
                    </div>
                </div>

                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>Эрозия шейки матки</h6>
                    </div>
                </div>

                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>Вагинальный кандидоз (молочница)</h6>
                    </div>
                </div>


                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>Нарушение менструального цикла</h6>
                    </div>
                </div>

                <div class="block-type">
                    <div id="coralcircle"></div>
                    <div class="type">
                        <h6>И многое другое</h6>
                    </div>
                </div>
            </div>

            <div class="banner" style="background-image:url(/wp-content/themes/init/img/banner.jpg);">
                <h6>Оставьте заявку и получите бесплатную консультацию по вашей проблеме</h6>
                <button class="btn">Оставить заявку</button>
            </div>
        </div>
    </div>
</section>

<section class="certificate-slider">
    <div class="container">
        <div class="wrapper">
            <div class="description">
                <h3>Сертификаты и обучение</h3>
                <div class="text">
                    <h4>Обучение</h4>
                    <p><i class="icon-diploma"></i>2003 г. - Закончила Карагандинский Государственный Медицинский Университет, лечебное дело, врач</p>
                </div>

                <div class="text">
                    <h4>Повышение квалификации</h4>
                    <p><i class="icon-cup"></i>27.06.2015 г. - Современные методы диагностики и лечения гинекологических заболеваний</p>
                    <p><i class="icon-cup"></i>29.12.2015 г. - Акушерство и гинекология</p>
                </div>
            </div>
            <div class="certificates">
                <div class="certificate">
                    <img src="/wp-content/themes/init/img/certificate1.jpg">
                </div>

                <div class="certificate">
                    <img src="/wp-content/themes/init/img/certificate2.jpg">
                </div>
            </div>

        </div>
    </div>
</section>

<section class="reviews">
    <div class="container">
        <div class="wrapper">
            <h3>Отзывы моих пациентов</h3>
            <div class="reviews-block">
                <div class="review">
                    <div class="review-header">
                        <div class="title">
                            <p>11 февраля 2021</p>
                            <h4>Анастасия</h4>
                        </div>
                        <div class="raiting">
                            <span>*****</span>
                        </div>
                    </div>
                    <p class="review-text">Мы с мужем хотим поблагодарить Цой Евгению Александровну. Это врач от Бога, к которму я хожу более двух лет. За этов ремя я у неё полностью вылечилась. Решила все проблемы, теперь жду ребеночка. От всей души желаю ей здоровья, успехов и всех благ. Таких врачей в наше время очень мало. Простое человеческое вам спасибо! С уважением, ваша пациентка Анастасия.</p>
                </div>

                <div class="review">
                    <div class="review-header">
                        <div class="title">
                            <p>23 декабря 2020</p>
                            <h4>Перизат</h4>
                        </div>
                        <div class="raiting">
                            <span>*****</span>
                        </div>
                    </div>
                    <p class="review-text">Спасибо большое Цой Евгения Александровне!!! Она опытный врач-гинеколог, всегда отзывчивая, просто у неё золотые руки. Успехов, здоровье и благополучие!!! Ещё раз огромное спасибо!!! С уважением, Перизат</p>
                </div>

                <div class="review">
                    <div class="review-header">
                        <div class="title">
                            <p>14 ноября 2020</p>
                            <h4>Санду</h4>
                        </div>
                        <div class="raiting">
                            <span>*****</span>
                        </div>
                    </div>
                    <p class="review-text">Спасибо Евгения Александровне!!! Очень высоко специализированный врач!!! Отвественно относится к своей работе и ко всем пациентам, все объясняет, успевает поддержать обратную связь. Желаю чтобы в жизни у нее все было хорошо!!! Успехов и здоровье ей!</p>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="questions">
    <div class="container">
        <div class="wrapper">
            <h3>Ответы на вопросы</h3>
            <div class="accordion">
                <div class="accord-item">
                    <div class="accord-header">
                        Вопрос<i class="icon-bottom"></i>
                    </div>

                    <div class="accord-content article">
                        <div class="answer">
                            <p>Ответ</p>
                        </div>
                    </div>
                </div>

                <div class="accord-item">
                    <div class="accord-header">
                        Вопрос<i class="icon-bottom"></i>
                    </div>

                    <div class="accord-content article">
                        <div class="answer">
                            <p>Ответ</p>
                        </div>
                    </div>
                </div>

                <div class="accord-item">
                    <div class="accord-header">
                        Вопрос<i class="icon-bottom"></i>
                    </div>

                    <div class="accord-content article">
                        <div class="answer">
                            <p>Ответ</p>
                        </div>
                    </div>
                </div>

                <div class="accord-item">
                    <div class="accord-header">
                        Вопрос<i class="icon-bottom"></i>
                    </div>

                    <div class="accord-content article">
                        <div class="answer">
                            <p>Ответ</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="banner" style="background-image:url(/wp-content/themes/init/img/banner.jpg);">
                <h6>Оставьте заявку и получите бесплатную консультацию по вашей проблеме</h6>
                <button class="btn">Оставить заявку</button>
            </div>
        </div>
    </div>
</section>

<section class="contacts">
    <div class="container">
        <div class="wrapper">
            <div class="block-form">
                <h5>Оставьте заявку</h5>
                <form id="order_form">
                    <div class="form-group">
                        <label for="user_name">Имя <span class="star">*</span></label>
                        <input type="text" id="user_name" class="required" placeholder="Имя">
                    </div>
                    <div class="form-group">
                        <label for="user_phone">Телефон <span class="star">*</span></label>
                        <input type="tel" id="user_phone" name="phone" class="phone-mask required" data-type="phone">
                    </div>
                    <div class="custom-checkbox form-group checkbox">
                        <input type="checkbox" id="check" class="checkbox-input required" data-type="checkbox" />
                        <label class="checkbox-label" for="check">Согласен на обработку персональных данных</label>
                    </div>
                    <button id="send_order" class="btn rounded">Записаться на прием</i></button>
                </form>
            </div>
            <div class="info">
                <h5>Мои контакты</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                <div class="social-networks">
                    <a><button class="network-btn"><i class="icon-instagram"></i></button></a>
                    <a><button class="network-btn"><i class="icon-whatsapp"></i></button></a>
                </div>

                <div class="map">
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A9717c7cb1e45ec128d68e7b6bd87e246fa4a5407a1798ad1669e59e0aab293a2&amp;width=500&amp;height=300&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer();
