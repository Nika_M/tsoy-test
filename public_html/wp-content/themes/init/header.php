<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package init
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header id="masthead" class="site-header">
		<div class="container">
			<div class="header-content">
				<div class="site-branding">
					<?php echo get_custom_logo() ? get_custom_logo() : "<a href='" . home_url() . "'><b>" . get_bloginfo("name") . "</b><h6>Гинеколог</h6></a>"; ?>

				</div>

				<nav>
					<ul>
						<li><a>Обо мне</a></li>
						<li><a>Болезни</a></li>
						<li><a>Лечение</a></li>
						<li><a>Вопросы и ответы</a></li>
					</ul>
				</nav>

				<div class="right-block">
					<a href="tel:77024041604"><button class="btn rounded">+7 777 777 77 77</button></a>
					<!-- <?php
							wp_nav_menu(array(
								'theme_location'    => 'primary',
								'container'         => 'nav',
								'menu_class'        => 'site-menu'
							));
							?> -->
				</div>

				<div class="open-mobile-menu icon-menu toggle-show outside-hide" data-group="mobile-menu" data-show="mobile-menu"></div>
			</div>
		</div>

		<div class="fixed-sidebar mobile-menu outside-hide" data-group="mobile-menu" data-show="mobile-menu">
			<div class="content article">
				<h3>Меню <div class="close-mobile-menu icon-left-arrow remove-show" data-show="mobile-menu"></div>
				</h3>
				<?php
				wp_nav_menu(array(
					'theme_location'    => 'primary',
					'container'         => 'nav',
					'menu_class'        => 'site-menu'
				));
				?>

				<div class="rs-contacts-us">
					<div class="item">
						<h3>Наши контакты</h3>
						<ul>
							<li><a href="tel:77024041604">+7 777 77 77</a></li>
							<li><a href="mailto:info@init.kz">info@init.kz</a></li>
						</ul>
					</div>
					<div class="item">
						<h3>Наш офис</h3>
						<p>г. Алматы, пр-т Абая 76/109<br> уг. ул. Ауэзова, 502 офис</p>
					</div>
				</div>
			</div>
		</div>
	</header>

	<main id="content" class="site-content">