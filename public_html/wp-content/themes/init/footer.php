<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package init
 */

?>

</main>

<?php get_sidebar(); ?>

<footer id="colophon" class="site-footer">

	<div class="popup error-popup">
		<div class="popup-wrapper">
			<div class="popup-blackboard"></div>
			<div class="popup-content">
				<div class="popup-header">
					Ошибка
					<div class="close-popup icon-close"></div>
				</div>
				<div class="popup-body center article">
					<h4>Упс!!!</h4>
					<p>На сайте произошла неизвестная ошибка</p>
					<div class="show-code link">показать код ошибки</div>
					<div class="error-code code-wrapper"></div>
					<button class="close-error-popup btn center">Закрыть</button>
				</div>
			</div>
		</div>
	</div>

	<div class="popup result-popup">
		<div class="popup-wrapper">
			<div class="popup-blackboard"></div>
			<div class="popup-content">
				<div class="popup-header">
					<span>Ошибка</span>
					<div class="close-popup icon-close"></div>
				</div>
				<div class="popup-body center article">
					<h4>Бла Бла</h4>
					<div class="icon">
						<div class="success icon-success"></div>
						<div class="error icon-error"></div>
						<div class="warning icon-warning"></div>
					</div>
					<p class="main">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quam, laudantium dolores dicta porro quaerat soluta doloribus numquam incidunt, nobis, fuga quae fugit ex alias.</p>
					<button class="close-popup btn center">Закрыть</button>
				</div>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="footer-top">
			<div class="site-branding">
				<?php echo get_custom_logo() ? get_custom_logo() : "<a href='" . home_url() . "'><b>" . get_bloginfo("name") . "</b></a>"; ?>
			</div>

			<nav>
				<ul>
					<li><a>Обо мне</a></li>
					<li><a>Болезни</a></li>
					<li><a>Лечение</a></li>
					<li><a>Вопросы и ответы</a></li>
				</ul>
			</nav>

			<div class="social-networks">
				<a><button class="network-btn"><i class="icon-instagram"></i></button></a>
				<a><button class="network-btn"><i class="icon-whatsapp"></i></button></a>
			</div>

			<!-- <?php
					wp_nav_menu(array(
						'theme_location'    => 'primary',
						'container'         => 'nav',
						'menu_class'        => 'site-menu'
					));
					?> -->
		</div>
		<div class="footer-bottom">
			<p>© <?php echo date('Y') . " - Гинеколог Алматы - " . get_bloginfo("name"); ?></p>
			<a href="https://init.kz" target="_blank" rel="noopener noreferrer">
				<p>Сайт разработала<br>Веб студия</p><img src="/wp-content/themes/init/img/init.svg" />
			</a>
		</div>
	</div>

	<?php wp_footer(); ?>

</footer>

</body>

</html>