<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'init_tsoytest' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'c(J#Sgxqvl?wyyuX9K,~Ry3gcJJ87NGUyM^iD=.`ofgz,):aYwQ8Hj]i hR5.}Cr' );
define( 'SECURE_AUTH_KEY',  '.j!g.ueWT9`~|sn@|Ne:sgI$dy_FnT9u_gds>U9D>-G46>fu2Q_:T3`nL~2I89Fu' );
define( 'LOGGED_IN_KEY',    '}>r@ftk,4)&>otI[7,|:qG7{x+,zK>AhjX0%8c%}+.Yx!KCW2p(UJdj{ T/J$}3L' );
define( 'NONCE_KEY',        'i,:KlBVy0TCBtEB?SxuOy!.lIl1/7l2%28He~xuFH%o%Ob}>AZ|w()FyG.)(Rvxr' );
define( 'AUTH_SALT',        'YM4g*JiM_}g9+y1oZ06OmrIh]]/&cS:##fEfo;Y=IE=/uW2fU85L8 &R4wDI(MsS' );
define( 'SECURE_AUTH_SALT', '$H-j:/mV,:Cfi!7;R`^cY9^UP4IG0k)=:c2Tpm$|Z2ta[DfDWU6wRzmdcHZD!>l0' );
define( 'LOGGED_IN_SALT',   'Dl>`3llGiw8jFv6?eO;)Q2k|>b<uSRlG~rTLLnHu//Vs^W):J+;Memc)77MX40-/' );
define( 'NONCE_SALT',       '6zS^%}nE))`wafhL-k{#_~SbcCO3~-L*Q*ojmD^ml;T/%GOg`@>!M=<G_Jv|8#4B' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'init_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
